package com.mastertech.marketplace.repositories;

import org.springframework.data.repository.CrudRepository;

import com.mastertech.marketplace.models.Product;
import com.mastertech.marketplace.models.User;

public interface ProductRepository extends CrudRepository<Product, Integer>{
	public Iterable<Product> findAllByOwner(User user);
}
